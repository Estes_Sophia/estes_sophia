#include <stdio.h>
#include <math.h>

void drawFlower();
void drawDog();
void drawArrow();
double triangleArea(double base, double vert, double hyp);
int main() {

int input;
scanf("%d", &input);


if(input == 1)
drawFlower();


if(input == 2)
drawDog();


 if(input == 3)
   drawArrow();

double base;
double vert;
double hyp;
scanf("%lf %lf %lf", &base, &vert, &hyp);


   double area = triangleArea(base, vert, hyp);
   
   if(area == -1.0)
      printf("Your inputs do not describe a right triangle!\n"); 
   
   else 
      printf("The area of a right triangle with a base side of %.0lf inches, a height of %.0lf inches, and a hypotenuse of %.0lf inches is %.2lf square inches.\n", base, vert, hyp, area);
      
    return 0;
}

void drawFlower()
{
printf("    _ _\n");
printf("   (_\\_)\n");
printf("  (__<_{}\n");
printf("   (_/_)\n");
printf("  |\\ |\n");
printf("   \\\\| /|\n");
printf("    \\|//\n");
printf("     |/\n");
printf(",.,.,|.,.,.\n");
printf("^`^`^`^`^`^\n");
}
void drawDog()
{
printf("^..^      /\n");
printf("/_/\\_____/\n");
printf("   /\\   /\\\n");
printf("  /  \\ /  \\\n");
}
void drawArrow()
{
   printf("**\n");
   printf("**\n");
   printf("**\n");
   printf("**\n");
   printf("**\n");
   printf("****\n");
   printf("***\n");
   printf("**\n");
   printf("*\n");
}
double triangleArea(double base, double vert, double hyp)
{
   double area;
   if ((base*base) + (vert*vert) == (hyp*hyp))
      area= ((1.0/2.0) * base * vert);
   else
      area = -1.0;
   
   return area;
}